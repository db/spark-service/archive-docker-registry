#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

docker login gitlab-registry.cern.ch/db/spark-service/docker-registry || exit 2

$DIR/spark-base/build && \
$DIR/spark-hadoop-xrootd/build && \
$DIR/spark-driver/build && \
$DIR/spark-executor/build && \
$DIR/spark-init/build && \
$DIR/spark-resource-staging-server/build && \
$DIR/spark-shuffle-service/build && \
$DIR/spark-edge/build || exit 2

docker push gitlab-registry.cern.ch/db/spark-service/docker-registry
