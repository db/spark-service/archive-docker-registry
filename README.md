# ARCHIVE - Docker registry (spark-on-k8s-2.2.0 fork)

**This repository holds all docker images required for spark 
on kubernetes. 
For details on their usage please go 
to [https://gitlab.cern.ch/db/spark-service/spark-service-tools](https://gitlab.cern.ch/db/spark-service/spark-service-tools)**

Base images
```
spark-base - base of Spark 
spark-hadoop-xrootd - base of Hadoop and Hadoop-XrootD-Connector
```

Spark on Kubernetes dependencies

```
spark-driver
spark-executor
spark-init
spark-resource-staging-server
spark-shuffle-service
```

### Documentation

**To build all spark on kubernetes images and push do registry**

```
./build-spark-deps.sh
```

**To build image with Dockerfile in e.g. `~/docker-registry/spark-base`** 


```
cd ~/docker-registry/spark-base
vim build # edit build file to adjust env variables
./build
```

**To browse `spark-base` image contents, e.g. `/opt/spark/dockerfiles` directory**

```
docker run -ti --rm -v /tmp/tmp gitlab-registry.cern.ch/db/spark-service/docker-registry/spark-base:latest bash
ls /opt/spark/dockerfiles
```

